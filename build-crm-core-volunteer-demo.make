api = 2
core = 7.23

projects[drupal][version] = 7.23

includes[] = drupal-org.make

; Download the install profile and recursively build all its dependencies
projects[crm_core_volunteer_demo][version] = 0.1
