<?php
/**
 * @file
 * crm_core_volunteer_sample_content.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function crm_core_volunteer_sample_content_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'site_frontpage';
  $strongarm->value = 'crm-core-volunteer-demo-front-page';
  $export['site_frontpage'] = $strongarm;

  return $export;
}
