<?php
/**
 * @file
 * crm_core_volunteer_sample_content.features.uuid_node.inc
 */

/**
 * Implements hook_uuid_features_default_content().
 */
function crm_core_volunteer_sample_content_uuid_features_default_content() {
  $nodes = array();

  $nodes[] = array(
  'uid' => 1,
  'title' => 'Work with Homeless Animals',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'cab1d30a-54b1-423c-8c90-b4c5ac7f90d4',
  'type' => 'cmcv_volunteer',
  'language' => 'und',
  'created' => 1379536652,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0b621efb-62fa-4312-b0bd-479c6d8d8be4',
  'revision_uid' => 1,
  'field_cmcv_contact' => array(),
  'field_cmcv_date' => array(
    'und' => array(
      0 => array(
        'value' => 1386099000,
        'value2' => 1386099000,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
  ),
  'field_cmcv_location' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => 'DC',
        'sub_administrative_area' => NULL,
        'locality' => 'Washington',
        'dependent_locality' => NULL,
        'postal_code' => '',
        'thoroughfare' => '',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_cmcv_roles' => array(
    'und' => array(
      0 => array(
        'value' => 'Animal Socializer, Adoption Counselor, Animal Care',
        'format' => NULL,
        'safe_value' => 'Animal Socializer, Adoption Counselor, Animal Care',
      ),
    ),
  ),
  'field_cmcv_slots' => array(
    'und' => array(
      0 => array(
        'value' => 36,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-18 20:37:32 +0000',
  'crm_core_profile_node_config' => array(
    'nid' => '0b621efb-62fa-4312-b0bd-479c6d8d8be4',
    'use_profile' => 1,
    'profile_name' => 'volunteer_commitment_form',
    'display_profile' => 1,
    'inline_title' => 'Volunteer form',
  ),
  'crm_core_volunteer_node_config' => array(
    'nid' => '0b621efb-62fa-4312-b0bd-479c6d8d8be4',
    'wait_list' => 1,
    'sign_email' => '',
    'wait_email' => '',
  ),
  'new_menu' => array(
    'menu_name' => 'main-menu',
    'mlid' => 619,
    'plid' => 0,
    'link_path' => 'node/5',
    'router_path' => 'node/%',
    'link_title' => 'Work with Homeless Animals',
    'options' => array(
      'identifier' => 'main-menu_work-with-homeless-animals:node/5',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'depth' => 1,
    'customized' => 0,
    'p1' => 619,
    'p2' => 0,
    'p3' => 0,
    'p4' => 0,
    'p5' => 0,
    'p6' => 0,
    'p7' => 0,
    'p8' => 0,
    'p9' => 0,
    'updated' => 0,
    'path' => 'node/%',
    'load_functions' => array(
      1 => 'node_load',
    ),
    'to_arg_functions' => '',
    'access_callback' => 'node_access',
    'access_arguments' => 'a:2:{i:0;s:4:"view";i:1;i:1;}',
    'page_callback' => 'node_page_view',
    'page_arguments' => 'a:1:{i:0;i:1;}',
    'delivery_callback' => '',
    'fit' => 2,
    'number_parts' => 2,
    'context' => 0,
    'tab_parent' => '',
    'tab_root' => 'node/%',
    'title' => 'Work with Homeless Animals',
    'title_callback' => 'node_page_title',
    'title_arguments' => 'a:1:{i:0;i:1;}',
    'theme_callback' => '',
    'theme_arguments' => 'a:0:{}',
    'type' => 6,
    'description' => '',
    'position' => '',
    'include_file' => '',
    'link_weight' => -45,
    'href' => 'node/5',
    'access' => TRUE,
    'localized_options' => array(
      'identifier' => 'main-menu_work-with-homeless-animals:node/5',
    ),
    'parent_depth_limit' => 8,
  ),
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Keep Our Creek Clean',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'e15d897f-9dbc-43c0-92e1-205be12acd18',
  'type' => 'cmcv_volunteer',
  'language' => 'und',
  'created' => 1379605238,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '0d86ea2a-fb8a-4ec5-8a00-3361772433d3',
  'revision_uid' => 1,
  'field_cmcv_contact' => array(),
  'field_cmcv_date' => array(
    'und' => array(
      0 => array(
        'value' => 1382796000,
        'value2' => 1382796000,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
  ),
  'field_cmcv_location' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => 'DC',
        'sub_administrative_area' => NULL,
        'locality' => 'Washington',
        'dependent_locality' => NULL,
        'postal_code' => '',
        'thoroughfare' => '',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_cmcv_roles' => array(),
  'field_cmcv_slots' => array(
    'und' => array(
      0 => array(
        'value' => 0,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-19 15:40:38 +0000',
  'crm_core_profile_node_config' => array(
    'nid' => '0d86ea2a-fb8a-4ec5-8a00-3361772433d3',
    'use_profile' => 1,
    'profile_name' => 'volunteer_commitment_form',
    'display_profile' => 1,
    'inline_title' => 'Volunteer form',
  ),
  'crm_core_volunteer_node_config' => array(
    'nid' => '0d86ea2a-fb8a-4ec5-8a00-3361772433d3',
    'wait_list' => 1,
    'sign_email' => '',
    'wait_email' => '',
  ),
  'new_menu' => array(
    'menu_name' => 'main-menu',
    'mlid' => 620,
    'plid' => 0,
    'link_path' => 'node/3',
    'router_path' => 'node/%',
    'link_title' => 'Keep Our Creek Clean',
    'options' => array(
      'identifier' => 'main-menu_keep-our-creek-clean:node/3',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'depth' => 1,
    'customized' => 0,
    'p1' => 620,
    'p2' => 0,
    'p3' => 0,
    'p4' => 0,
    'p5' => 0,
    'p6' => 0,
    'p7' => 0,
    'p8' => 0,
    'p9' => 0,
    'updated' => 0,
    'path' => 'node/%',
    'load_functions' => array(
      1 => 'node_load',
    ),
    'to_arg_functions' => '',
    'access_callback' => 'node_access',
    'access_arguments' => 'a:2:{i:0;s:4:"view";i:1;i:1;}',
    'page_callback' => 'node_page_view',
    'page_arguments' => 'a:1:{i:0;i:1;}',
    'delivery_callback' => '',
    'fit' => 2,
    'number_parts' => 2,
    'context' => 0,
    'tab_parent' => '',
    'tab_root' => 'node/%',
    'title' => 'Keep Our Creek Clean',
    'title_callback' => 'node_page_title',
    'title_arguments' => 'a:1:{i:0;i:1;}',
    'theme_callback' => '',
    'theme_arguments' => 'a:0:{}',
    'type' => 6,
    'description' => '',
    'position' => '',
    'include_file' => '',
    'link_weight' => -47,
    'href' => 'node/3',
    'access' => TRUE,
    'localized_options' => array(
      'identifier' => 'main-menu_keep-our-creek-clean:node/3',
    ),
    'parent_depth_limit' => 8,
  ),
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'After School Tutors Needed',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => 'f4648f85-4015-4d15-b774-da11f67c2249',
  'type' => 'cmcv_volunteer',
  'language' => 'und',
  'created' => 1379606513,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '2a0e3955-18a5-4be1-a2c4-8dff6e05be73',
  'revision_uid' => 1,
  'field_cmcv_contact' => array(
    'und' => array(
      0 => array(
        'value' => 'heytrellon@trellon.com',
        'format' => NULL,
        'safe_value' => 'heytrellon@trellon.com',
      ),
    ),
  ),
  'field_cmcv_date' => array(
    'und' => array(
      0 => array(
        'value' => 1378148400,
        'value2' => 1387396800,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
  ),
  'field_cmcv_location' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => 'MA',
        'sub_administrative_area' => NULL,
        'locality' => 'Boston',
        'dependent_locality' => NULL,
        'postal_code' => '',
        'thoroughfare' => 'Chestnut Hill Elementary School',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_cmcv_roles' => array(),
  'field_cmcv_slots' => array(
    'und' => array(
      0 => array(
        'value' => 14,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-19 16:01:53 +0000',
  'crm_core_profile_node_config' => array(
    'nid' => '2a0e3955-18a5-4be1-a2c4-8dff6e05be73',
    'use_profile' => 1,
    'profile_name' => 'volunteer_commitment_form_2',
    'display_profile' => 1,
    'inline_title' => 'Volunteer form',
  ),
  'crm_core_volunteer_node_config' => array(
    'nid' => '2a0e3955-18a5-4be1-a2c4-8dff6e05be73',
    'wait_list' => 1,
    'sign_email' => '',
    'wait_email' => '',
  ),
  'new_menu' => array(
    'menu_name' => 'main-menu',
    'mlid' => 622,
    'plid' => 0,
    'link_path' => 'node/1',
    'router_path' => 'node/%',
    'link_title' => 'After School Tutors Needed',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'depth' => 1,
    'customized' => 0,
    'p1' => 622,
    'p2' => 0,
    'p3' => 0,
    'p4' => 0,
    'p5' => 0,
    'p6' => 0,
    'p7' => 0,
    'p8' => 0,
    'p9' => 0,
    'updated' => 0,
    'path' => 'node/%',
    'load_functions' => array(
      1 => 'node_load',
    ),
    'to_arg_functions' => '',
    'access_callback' => 'node_access',
    'access_arguments' => 'a:2:{i:0;s:4:"view";i:1;i:1;}',
    'page_callback' => 'node_page_view',
    'page_arguments' => 'a:1:{i:0;i:1;}',
    'delivery_callback' => '',
    'fit' => 2,
    'number_parts' => 2,
    'context' => 0,
    'tab_parent' => '',
    'tab_root' => 'node/%',
    'title' => 'After School Tutors Needed',
    'title_callback' => 'node_page_title',
    'title_arguments' => 'a:1:{i:0;i:1;}',
    'theme_callback' => '',
    'theme_arguments' => 'a:0:{}',
    'type' => 6,
    'description' => '',
    'position' => '',
    'include_file' => '',
    'link_weight' => -49,
    'href' => 'node/1',
    'access' => TRUE,
    'localized_options' => array(),
    'parent_depth_limit' => 8,
  ),
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Get Out The Vote!',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '0dc3a247-ae22-4854-b9fb-e2d3c647b96f',
  'type' => 'cmcv_volunteer',
  'language' => 'und',
  'created' => 1379605567,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '85b8792e-c6f0-41e1-bb04-7cbf6b522abf',
  'revision_uid' => 1,
  'field_cmcv_contact' => array(
    'und' => array(
      0 => array(
        'value' => 'heytrellon@trellon.com',
        'format' => NULL,
        'safe_value' => 'heytrellon@trellon.com',
      ),
    ),
  ),
  'field_cmcv_date' => array(
    'und' => array(
      0 => array(
        'value' => 1379602800,
        'value2' => 1383584400,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
  ),
  'field_cmcv_location' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => '',
        'sub_administrative_area' => NULL,
        'locality' => '',
        'dependent_locality' => NULL,
        'postal_code' => '',
        'thoroughfare' => '',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_cmcv_roles' => array(),
  'field_cmcv_slots' => array(
    'und' => array(
      0 => array(
        'value' => 100,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-19 15:46:07 +0000',
  'crm_core_profile_node_config' => array(
    'nid' => '85b8792e-c6f0-41e1-bb04-7cbf6b522abf',
    'use_profile' => 1,
    'profile_name' => 'volunteer_commitment_form',
    'display_profile' => 1,
    'inline_title' => 'Volunteer form',
  ),
  'crm_core_volunteer_node_config' => array(
    'nid' => '85b8792e-c6f0-41e1-bb04-7cbf6b522abf',
    'wait_list' => 1,
    'sign_email' => '',
    'wait_email' => '',
  ),
  'new_menu' => array(
    'menu_name' => 'main-menu',
    'mlid' => 621,
    'plid' => 0,
    'link_path' => 'node/2',
    'router_path' => 'node/%',
    'link_title' => 'Get Out The Vote!',
    'options' => array(),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'depth' => 1,
    'customized' => 0,
    'p1' => 621,
    'p2' => 0,
    'p3' => 0,
    'p4' => 0,
    'p5' => 0,
    'p6' => 0,
    'p7' => 0,
    'p8' => 0,
    'p9' => 0,
    'updated' => 0,
    'path' => 'node/%',
    'load_functions' => array(
      1 => 'node_load',
    ),
    'to_arg_functions' => '',
    'access_callback' => 'node_access',
    'access_arguments' => 'a:2:{i:0;s:4:"view";i:1;i:1;}',
    'page_callback' => 'node_page_view',
    'page_arguments' => 'a:1:{i:0;i:1;}',
    'delivery_callback' => '',
    'fit' => 2,
    'number_parts' => 2,
    'context' => 0,
    'tab_parent' => '',
    'tab_root' => 'node/%',
    'title' => 'Get Out The Vote!',
    'title_callback' => 'node_page_title',
    'title_arguments' => 'a:1:{i:0;i:1;}',
    'theme_callback' => '',
    'theme_arguments' => 'a:0:{}',
    'type' => 6,
    'description' => '',
    'position' => '',
    'include_file' => '',
    'link_weight' => -48,
    'href' => 'node/2',
    'access' => TRUE,
    'localized_options' => array(),
    'parent_depth_limit' => 8,
  ),
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Support the Food Bank',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '85aa7335-b5cd-4352-a820-5cfbcbd6fe51',
  'type' => 'cmcv_volunteer',
  'language' => 'und',
  'created' => 1379605986,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => '9c84b384-91ef-438d-a442-b7aa4158823f',
  'revision_uid' => 1,
  'field_cmcv_contact' => array(),
  'field_cmcv_date' => array(
    'und' => array(
      0 => array(
        'value' => 1387404000,
        'value2' => 1387404000,
        'timezone' => 'UTC',
        'timezone_db' => 'UTC',
        'date_type' => 'datestamp',
      ),
    ),
  ),
  'field_cmcv_location' => array(
    'und' => array(
      0 => array(
        'country' => 'US',
        'administrative_area' => 'NY',
        'sub_administrative_area' => NULL,
        'locality' => 'New York',
        'dependent_locality' => NULL,
        'postal_code' => '',
        'thoroughfare' => '',
        'premise' => '',
        'sub_premise' => NULL,
        'organisation_name' => NULL,
        'name_line' => NULL,
        'first_name' => NULL,
        'last_name' => NULL,
        'data' => NULL,
      ),
    ),
  ),
  'field_cmcv_roles' => array(),
  'field_cmcv_slots' => array(
    'und' => array(
      0 => array(
        'value' => 2,
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'sioc:Item',
      1 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-19 15:53:06 +0000',
  'crm_core_profile_node_config' => array(
    'nid' => '9c84b384-91ef-438d-a442-b7aa4158823f',
    'use_profile' => 1,
    'profile_name' => 'volunteer_commitment_form',
    'display_profile' => 1,
    'inline_title' => 'Volunteer form',
  ),
  'crm_core_volunteer_node_config' => array(
    'nid' => '9c84b384-91ef-438d-a442-b7aa4158823f',
    'wait_list' => 1,
    'sign_email' => '',
    'wait_email' => '',
  ),
  'new_menu' => array(
    'menu_name' => 'main-menu',
    'mlid' => 618,
    'plid' => 0,
    'link_path' => 'node/4',
    'router_path' => 'node/%',
    'link_title' => 'Support the Food Bank',
    'options' => array(
      'identifier' => 'main-menu_support-the-food-bank:node/4',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'depth' => 1,
    'customized' => 0,
    'p1' => 618,
    'p2' => 0,
    'p3' => 0,
    'p4' => 0,
    'p5' => 0,
    'p6' => 0,
    'p7' => 0,
    'p8' => 0,
    'p9' => 0,
    'updated' => 0,
    'path' => 'node/%',
    'load_functions' => array(
      1 => 'node_load',
    ),
    'to_arg_functions' => '',
    'access_callback' => 'node_access',
    'access_arguments' => 'a:2:{i:0;s:4:"view";i:1;i:1;}',
    'page_callback' => 'node_page_view',
    'page_arguments' => 'a:1:{i:0;i:1;}',
    'delivery_callback' => '',
    'fit' => 2,
    'number_parts' => 2,
    'context' => 0,
    'tab_parent' => '',
    'tab_root' => 'node/%',
    'title' => 'Support the Food Bank',
    'title_callback' => 'node_page_title',
    'title_arguments' => 'a:1:{i:0;i:1;}',
    'theme_callback' => '',
    'theme_arguments' => 'a:0:{}',
    'type' => 6,
    'description' => '',
    'position' => '',
    'include_file' => '',
    'link_weight' => -46,
    'href' => 'node/4',
    'access' => TRUE,
    'localized_options' => array(
      'identifier' => 'main-menu_support-the-food-bank:node/4',
    ),
    'parent_depth_limit' => 8,
  ),
);
  $nodes[] = array(
  'uid' => 1,
  'title' => 'Thank you',
  'log' => '',
  'status' => 1,
  'comment' => 1,
  'promote' => 0,
  'sticky' => 0,
  'vuuid' => '60861c83-d850-44fd-b63b-ec4a58ce2dd4',
  'type' => 'page',
  'language' => 'und',
  'created' => 1378725306,
  'tnid' => 0,
  'translate' => 0,
  'uuid' => 'a6aca833-1f84-4876-93d1-cc070aad77d0',
  'revision_uid' => 1,
  'body' => array(
    'und' => array(
      0 => array(
        'value' => 'Thank you for your volunteer commitment!',
        'summary' => '',
        'format' => 'filtered_html',
        'safe_value' => '<p>Thank you for your volunteer commitment!</p>
',
        'safe_summary' => '',
      ),
    ),
  ),
  'rdf_mapping' => array(
    'rdftype' => array(
      0 => 'foaf:Document',
    ),
    'title' => array(
      'predicates' => array(
        0 => 'dc:title',
      ),
    ),
    'created' => array(
      'predicates' => array(
        0 => 'dc:date',
        1 => 'dc:created',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'changed' => array(
      'predicates' => array(
        0 => 'dc:modified',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
    'body' => array(
      'predicates' => array(
        0 => 'content:encoded',
      ),
    ),
    'uid' => array(
      'predicates' => array(
        0 => 'sioc:has_creator',
      ),
      'type' => 'rel',
    ),
    'name' => array(
      'predicates' => array(
        0 => 'foaf:name',
      ),
    ),
    'comment_count' => array(
      'predicates' => array(
        0 => 'sioc:num_replies',
      ),
      'datatype' => 'xsd:integer',
    ),
    'last_activity' => array(
      'predicates' => array(
        0 => 'sioc:last_activity_date',
      ),
      'datatype' => 'xsd:dateTime',
      'callback' => 'date_iso8601',
    ),
  ),
  'cid' => 0,
  'last_comment_name' => NULL,
  'last_comment_uid' => 1,
  'comment_count' => 0,
  'name' => 'admin',
  'picture' => 0,
  'data' => 'a:1:{s:17:"mimemail_textonly";i:0;}',
  'date' => '2013-09-09 11:15:06 +0000',
  'url_alias' => 'thank-you',
);
  return $nodes;
}
