<?php
/**
 * @file
 * Sample data for CRM Core Donation Demo install profile.
 */

/**
 * Sample data for CRM Core Donation Demo install profile.
 */
function crm_core_volunteer_demo_sample_data() {

  _crm_core_volunteer_sample_content_rebuild_nodes();
  $uuids = array(
    1 => '2a0e3955-18a5-4be1-a2c4-8dff6e05be73',
    2 => '85b8792e-c6f0-41e1-bb04-7cbf6b522abf',
    3 => '0d86ea2a-fb8a-4ec5-8a00-3361772433d3',
    4 => '9c84b384-91ef-438d-a442-b7aa4158823f',
    5 => '0b621efb-62fa-4312-b0bd-479c6d8d8be4',
  );

  // Getting local IDs from universal.
  $nids = entity_get_id_by_uuid('node', $uuids);

  $sample_data = array (
    'crm_core_contact' =>
    array (
      2 => '{
  "vid" : "16",
  "log" : "",
  "created" : "1377557220",
  "changed" : "1378480551",
  "uid" : "1",
  "contact_id" : "2",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Thomas",
        "middle" : "Ruggles",
        "family" : "Pynchon",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Thomas",
          "middle" : "Ruggles",
          "family" : "Pynchon",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1962-08-14 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "pynchont@yoyodyne.com" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "CT",
        "sub_administrative_area" : null,
        "locality" : "Cudworth",
        "dependent_locality" : null,
        "postal_code" : "06818-8100",
        "thoroughfare" : "5972 Quiet Blossom Circuit",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "2037029917", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      3 => '{
  "vid" : "21",
  "log" : "",
  "created" : "1377557222",
  "changed" : "1378481066",
  "uid" : "1",
  "contact_id" : "3",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Jonathan",
        "middle" : "Simmons",
        "family" : "Barth",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Jonathan",
          "middle" : "Simmons",
          "family" : "Barth",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1995-09-06 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "john.barth@fiveseasons.com" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "IN",
        "sub_administrative_area" : null,
        "locality" : "Antler",
        "dependent_locality" : null,
        "postal_code" : "46808-4556",
        "thoroughfare" : "6488 Red Place",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "7654802886", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      4 => '{
  "vid" : "15",
  "log" : "",
  "created" : "1377557224",
  "changed" : "1378480441",
  "uid" : "1",
  "contact_id" : "4",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "David",
        "middle" : "Foster",
        "family" : "Wallace",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "David",
          "middle" : "Foster",
          "family" : "Wallace",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1982-10-13 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "david.wallace5@peoria.irs.gov" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "OR",
        "sub_administrative_area" : null,
        "locality" : "Curfew",
        "dependent_locality" : null,
        "postal_code" : "97423-6608",
        "thoroughfare" : "2619 Quaking Leaf Farms",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "9719273347", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      5 => '{
  "vid" : "20",
  "log" : "",
  "created" : "1377557227",
  "changed" : "1378480936",
  "uid" : "1",
  "contact_id" : "5",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Hansel",
        "middle" : "Friedrich",
        "family" : "Fallada",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Hansel",
          "middle" : "Friedrich",
          "family" : "Fallada",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1978-12-22 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "falladah@dp-dhl.com" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "CT",
        "sub_administrative_area" : null,
        "locality" : "Marquis",
        "dependent_locality" : null,
        "postal_code" : "06945-7254",
        "thoroughfare" : "3289 Heather Beacon Pathway",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "4751547833", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      6 => '{
  "vid" : "14",
  "log" : "",
  "created" : "1377557229",
  "changed" : "1378480314",
  "uid" : "1",
  "contact_id" : "6",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Roland",
        "middle" : "Gerard",
        "family" : "Barthes",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Roland",
          "middle" : "Gerard",
          "family" : "Barthes",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1990-02-21 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "rgb@mythologies.fr" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "TX",
        "sub_administrative_area" : null,
        "locality" : "Oldrag",
        "dependent_locality" : null,
        "postal_code" : "78028-2555",
        "thoroughfare" : "634 Hazy Robin Pike",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "9033973316", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      7 => '{
  "vid" : "19",
  "log" : "",
  "created" : "1377557231",
  "changed" : "1378480839",
  "uid" : "1",
  "contact_id" : "7",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Tom",
        "middle" : "Coraghessan",
        "family" : "Boyle",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Tom",
          "middle" : "Coraghessan",
          "family" : "Boyle",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1984-11-07 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "tcboyle@usc.edu" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "OR",
        "sub_administrative_area" : null,
        "locality" : "Tip Top",
        "dependent_locality" : null,
        "postal_code" : "97822-9789",
        "thoroughfare" : "9170 Merry Creek Way",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "4584319059", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      8 => '{
  "vid" : "13",
  "log" : "",
  "created" : "1377557233",
  "changed" : "1378480183",
  "uid" : "1",
  "contact_id" : "8",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Stephen",
        "middle" : "Michael",
        "family" : "Erickson",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Stephen",
          "middle" : "Michael",
          "family" : "Erickson",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1989-06-13 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "bigfan@allmovies.com" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "IN",
        "sub_administrative_area" : null,
        "locality" : "Third Cliff",
        "dependent_locality" : null,
        "postal_code" : "47587-6597",
        "thoroughfare" : "4242 Rustic Rabbit Canyon",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "3171391106", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      9 => '{
  "vid" : "18",
  "log" : "",
  "created" : "1377557235",
  "changed" : "1378480747",
  "uid" : "1",
  "contact_id" : "9",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Bill",
        "middle" : "Ford",
        "family" : "Gibson",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Bill",
          "middle" : "Ford",
          "family" : "Gibson",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1987-08-29 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "gibsonb@wintermute.it" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "WV",
        "sub_administrative_area" : null,
        "locality" : "Black Hog Landing",
        "dependent_locality" : null,
        "postal_code" : "26185-3141",
        "thoroughfare" : "2487 Pleasant Apple Ramp",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "3042488587", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      10 => '{
  "vid" : "12",
  "log" : "",
  "created" : "1377557237",
  "changed" : "1378479916",
  "uid" : "1",
  "contact_id" : "10",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "Marcus",
        "middle" : "Zachary",
        "family" : "Danielewski",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "Marcus",
          "middle" : "Zachary",
          "family" : "Danielewski",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1987-03-11 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "mzd@50year.com" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "IN",
        "sub_administrative_area" : null,
        "locality" : "Yarbo",
        "dependent_locality" : null,
        "postal_code" : "47869-8009",
        "thoroughfare" : "5986 Amber Island Bank",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "7651989087", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
      11 => '{
  "vid" : "17",
  "log" : "",
  "created" : "1377557240",
  "changed" : "1378480632",
  "uid" : "1",
  "contact_id" : "11",
  "type" : "individual",
  "default_revision" : "1",
  "contact_name" : { "und" : [
      {
        "title" : "",
        "given" : "William",
        "middle" : "Thomas",
        "family" : "Gaddis",
        "generational" : "",
        "credentials" : "",
        "safe" : {
          "title" : "",
          "given" : "William",
          "middle" : "Thomas",
          "family" : "Gaddis",
          "generational" : "",
          "credentials" : ""
        }
      }
    ]
  },
  "field_ao_birthday" : { "und" : [
      {
        "value" : "1994-06-14 00:00:00",
        "timezone" : "UTC",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_ao_business_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "",
        "sub_administrative_area" : null,
        "locality" : "",
        "dependent_locality" : null,
        "postal_code" : "",
        "thoroughfare" : "",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_email_address" : { "und" : [ { "email" : "gaddis.william@carpenters.org" } ] },
  "field_ao_facebook" : [],
  "field_ao_home_address" : { "und" : [
      {
        "country" : "US",
        "administrative_area" : "NE",
        "sub_administrative_area" : null,
        "locality" : "Merry Midnight",
        "dependent_locality" : null,
        "postal_code" : "68257-1715",
        "thoroughfare" : "587 Shady Wagon Lookout",
        "premise" : "",
        "sub_premise" : null,
        "organisation_name" : null,
        "name_line" : null,
        "first_name" : null,
        "last_name" : null,
        "data" : null
      }
    ]
  },
  "field_ao_primary_telephone" : { "und" : [ { "number" : "3082228223", "country_codes" : "us", "extension" : "" } ] },
  "field_ao_twitter" : [],
  "rdf_mapping" : []
}',
    ),
    'crm_core_activity' =>
    array (
      11 => '{
  "revision_id" : "11",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Support the Food Bank\\u0022",
  "created" : "1379606020",
  "changed" : "1379606212",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "4" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:00:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : { "und" : [
      {
        "value" : "I\\u0027m excited to help out!",
        "format" : null,
        "safe_value" : "I\\u0026#039;m excited to help out!"
      }
    ]
  },
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:00:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[4]] . '" } ] },
  "rdf_mapping" : []
}',
      12 => '{
  "revision_id" : "14",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Support the Food Bank\\u0022",
  "created" : "1379606128",
  "changed" : "1379606324",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "6" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:00:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:00:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[4]] . '" } ] },
  "rdf_mapping" : []
}',
      23 => '{
  "revision_id" : "25",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Support the Food Bank\\u0022",
  "created" : "1379608711",
  "changed" : "1379608711",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "7" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:38:31",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "wait list" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[4]] . '" } ] },
  "rdf_mapping" : []
}',
      24 => '{
  "revision_id" : "26",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Work with Homeless Animals\\u0022",
  "created" : "1379608731",
  "changed" : "1379608731",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "7" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:38:51",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[5]] . '" } ] },
  "rdf_mapping" : []
}',
      25 => '{
  "revision_id" : "27",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Get Out The Vote!\\u0022",
  "created" : "1379608753",
  "changed" : "1379608753",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "7" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:39:14",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[2]] . '" } ] },
  "rdf_mapping" : []
}',
      26 => '{
  "revision_id" : "28",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022After School Tutors Needed\\u0022",
  "created" : "1379608770",
  "changed" : "1379608770",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "7" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:39:30",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "attended" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "Walk Up" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[1]] . '" } ] },
  "rdf_mapping" : []
}',
      27 => '{
  "revision_id" : "29",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022After School Tutors Needed\\u0022",
  "created" : "1379608841",
  "changed" : "1379608841",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "2" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:40:41",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[1]] . '" } ] },
  "rdf_mapping" : []
}',
      28 => '{
  "revision_id" : "30",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Work with Homeless Animals\\u0022",
  "created" : "1379608861",
  "changed" : "1379608861",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "2" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:41:01",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[5]] . '" } ] },
  "rdf_mapping" : []
}',
      29 => '{
  "revision_id" : "31",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Keep Our Creek Clean\\u0022",
  "created" : "1379608889",
  "changed" : "1379608889",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "5" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:41:29",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[3]] . '" } ] },
  "rdf_mapping" : []
}',
      30 => '{
  "revision_id" : "32",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Get Out The Vote!\\u0022",
  "created" : "1379608915",
  "changed" : "1379608915",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "10" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:41:55",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[2]] . '" } ] },
  "rdf_mapping" : []
}',
      31 => '{
  "revision_id" : "33",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Work with Homeless Animals\\u0022",
  "created" : "1379608954",
  "changed" : "1379608954",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "4" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:42:34",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[5]] . '" } ] },
  "rdf_mapping" : []
}',
      32 => '{
  "revision_id" : "34",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Keep Our Creek Clean\\u0022",
  "created" : "1379608984",
  "changed" : "1379608984",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "6" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:43:04",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[3]] . '" } ] },
  "rdf_mapping" : []
}',
      33 => '{
  "revision_id" : "35",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022After School Tutors Needed\\u0022",
  "created" : "1379609026",
  "changed" : "1379609026",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "9" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:43:47",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[1]] . '" } ] },
  "rdf_mapping" : []
}',
      34 => '{
  "revision_id" : "36",
  "uid" : "1",
  "title" : "Commitment for Volunteer Opportunity to \\u0022Keep Our Creek Clean\\u0022",
  "created" : "1379609044",
  "changed" : "1379609044",
  "type" : "cmcv_volunteer_commitment",
  "default_revision" : "1",
  "field_activity_participants" : { "und" : [ { "target_id" : "9" } ] },
  "field_activity_date" : { "und" : [
      {
        "value" : "2013-09-19 16:44:04",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_activity_notes" : [],
  "field_cmcv_reg_date" : { "und" : [
      {
        "value" : "2013-09-19 12:45:00",
        "timezone" : "America\\/New_York",
        "timezone_db" : "UTC",
        "date_type" : "datetime"
      }
    ]
  },
  "field_cmcv_signup_role" : [],
  "field_cmcv_signup_status" : { "und" : [ { "value" : "registered" } ] },
  "field_cmcv_source" : { "und" : [ { "value" : "On-line registration" } ] },
  "field_cmcv_vo_reference" : { "und" : [ { "target_id" : "' . $nids[$uuids[3]] . '" } ] },
  "rdf_mapping" : []
}',
    ),
  );

  return $sample_data;
}
